;; required packages

(prelude-require-packages '(calfw calfw-org visual-regexp comment-dwim-2 org-contrib org-contacts org-caldav))
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e/") ;; to locate mu4e
(require 'mu4e) ;; require as we need the function for configuration below


;; slime, for lisp editing and swank
(setq inferior-lisp-program "/usr/bin/sbcl")
(add-to-list 'load-path "/usr/share/emacs/site-lisp/slime/")
(require 'slime)
(slime-setup)

;; useful global shortcuts

(define-key prelude-mode-map (kbd "C-c A") 'cfw:open-org-calendar)
(define-key prelude-mode-map (kbd "C-c m") 'mu4e)
(define-key prelude-mode-map (kbd "C-c R") 'crux-rename-buffer-and-file)
(define-key prelude-mode-map (kbd "C-c r") 'vr/replace)
(define-key prelude-mode-map (kbd "C-c t") 'org-capture)
(global-set-key (kbd "M-;") 'comment-dwim-2)


;; autoloads

(autoload 'cfw:open-org-calendar "calfw-org" "nice calendar viewer" t)

;; general configuration

;; disable scroll bar
(toggle-scroll-bar -1)
;; disable menu bar
(menu-bar-mode -1)
;; 80 is an acceptable line length
(setq-default fill-column 80)
;; prelude whitespace is annoying to me
(setq prelude-whitespace nil)
;; use word diff on the current magit diff
(setq magit-diff-refine-hunk t)
;; allow to move nicely in helm mini buffer groups
(setq helm-move-to-line-cycle-in-source nil)
;; ace window keys
(setq aw-keys '(?a ?i ?e ?s ?r ?, ?é ?l ?d))
;; no real need for that, and it currently bother me to see the messages of
;; ~undo-tree~ files being
(setq undo-tree-auto-save-history nil)

(setq comment-dwim-2--inline-comment-behavior 'reindent-comment)

(when (fboundp 'imagemagick-register-types)
  (imagemagick-register-types))

;; override the prelude function to disable smartparens-strct-mode
(defun prelude-lisp-coding-defaults ()
  (rainbow-delimiters-mode +1))

;; reverse the use of TAB and C-z in helm completion, to have TAB completion
(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
(define-key helm-map (kbd "TAB") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z") 'helm-select-action)

;; org configuration

(setq org-agenda-default-appointment-duration 30)
(setq org-agenda-files (list (expand-file-name "~/Documents/org/agenda"))
      ;; timezone
      org-icalendar-timezone "Europe/Paris"
      ;; show next two weks and three days before current date
      org-agenda-span 18
      org-agenda-start-on-weekday nil
      org-agenda-start-day "-3d")
(setq org-habit-show-habits-only-for-today nil)

;; pour ne pas surcharger `org-archive-subtree'
(with-eval-after-load "flyspell"
  (define-key flyspell-mode-map (kbd "C-c $") nil))
(with-eval-after-load "org"
  (define-key org-mode-map (kbd "C-c <tab>") #'org-global-cycle))

;; org-contacts configuration

;; required for integration in org agenda, otherwise I get
;; org-contacts-anniversaries: bad sexp
(setq org-contacts-enable-completion nil) ; mu4e completion is better
(require 'org-contacts)
(require 'diary-lib)

;; org-contacts-files wants explicitely files, not a directory
(setq org-contacts-files (directory-files (expand-file-name "~/Documents/org/contacts") t ".*\.org")
      org-contacts-birthday-format "Anniversaire: %l (%y ans)"
      org-contacts-property-values-separators " +; +")

(setq org-capture-templates
      '(("t" "Todo"
         entry (file+headline "~/Documents/org/agenda/todos.org" "Todos")
         "* TODO %?\n  Noté le %u" :empty-lines-before 1)
        ("T" "Todo with link from current buffer"
         entry (file+headline "~/Documents/org/agenda/todos.org" "Todos")
         "* TODO %?\n  Noté le %u\n  %a" :empty-lines-before 1)
        ("a" "Agenda"
         entry (file+headline "~/Documents/org/agenda/agenda.org" "Agenda")
         "* %?\n  %^t\n  %i" :empty-lines-before 1)
        ("A" "Agenda with link from current buffer"
         entry (file+headline "~/Documents/org/agenda/agenda.org" "Agenda")
         "* %?\n  %^t\n  %i\n  %a" :empty-lines-before 1)))

(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (sqlite . t)
   (shell . t)
   (python . t)
   ;; Include other languages here...
   ))

(setq org-src-fontify-natively t)
;; Don't prompt before running code in org
(setq org-confirm-babel-evaluate nil)


;; mu4e configuration

(defun jp/mu4e-update ()
  "show mu4e and start mail update"
  (interactive)
  (mu4e)
  (mu4e-update-mail-and-index nil))

(defun my-mu4e-refile-folder-function (msg)
  "Set the refile folder for MSG."
  (format "/Archive/%s" (format-time-string "%Y" (mu4e-message-field msg :date))))

(setq mu4e-get-mail-command "mbsync -a"  ;; command to fetch mails
      user-full-name "Julien Pagès"
      message-citation-line-format "%a %d %b %Y à %R, %f a écrit :\n"
      message-citation-line-function 'message-insert-formatted-citation-line
      mu4e-compose-signature "Julien Pagès"

      ;; Some IMAP-synchronization programs such as mbsync (but not offlineimap)
      ;; don’t like it when message files do not change their names when they
      ;; are moved to different folders.
      mu4e-change-filenames-when-moving nil

      ;; configuration to send mail
      sendmail-program "/usr/bin/msmtp"
      send-mail-function 'smtpmail-send-it
      message-sendmail-f-is-evil t
      message-sendmail-extra-arguments '("--read-envelope-from")
      message-send-mail-function 'message-send-mail-with-sendmail

      ;; show images inline

      ;; defined with contexts, see below
      mu4e-sent-folder   nil  ;; folder for sent messages
      mu4e-drafts-folder nil  ;; unfinished messages
      mu4e-trash-folder  nil  ;; trashed messages
      mu4e-refile-folder nil  ;; saved messages
      user-mail-address  nil

      mu4e-headers-fields '((:human-date . 12)
                            (:flags . 6)
                            (:from-or-to . 22)
                            (:subject . nil))

      mu4e-contexts `(,(make-mu4e-context
                        :name "posteo"
                        :match-func (lambda (msg) (when msg
                                                    (string-prefix-p "/Posteo" (mu4e-message-field msg :maildir))))
                        :vars '((user-mail-address  . "julien.pages@posteo.net")
                                (mu4e-sent-folder   . "/Posteo/Sent")
                                (mu4e-drafts-folder . "/Posteo/Drafts")
                                (mu4e-trash-folder  . "/Posteo/Trash")
                                (mu4e-sent-messages-behavior . sent)))
                      ,(make-mu4e-context
                        :name "gmail"
                        :match-func (lambda (msg) (when msg
                                                    (string-prefix-p "/Gmail" (mu4e-message-field msg :maildir))))
                        :vars '((user-mail-address  . "j.parkouss@gmail.com")
                                (mu4e-sent-folder   . "/Gmail/Sent")
                                (mu4e-drafts-folder . "/Gmail/Drafts")
                                (mu4e-trash-folder  . "/Gmail/Trash")
                                ;; don't send messages to Sent, GMail/IMAP takes care of this
                                (mu4e-sent-messages-behavior . delete))))

      mu4e-context-policy 'pick-first   ;; start with the first context
      mu4e-compose-context-policy 'pick-first  ;; always use first context to compose mail
      mu4e-refile-folder 'my-mu4e-refile-folder-function)

(setq mu4e-bookmarks
      '(( :name  "Messages non lus"
          :query "flag:unread AND NOT flag:trashed"
          :key ?u)
        ( :name "Messages du jour"
          :query "date:today..now"
          :key ?t)
        ( :name "Messages des 7 derniers jours"
          :query "date:7d..now"
          :hide-unread t
          :key ?w)
        ( :name "Messages à supprimer ou «refiler» de Posteo"
          :query "maildir:/Posteo* AND date:1900..today and flag:attach"
          :key ?k
          :hide-unread t)))

;; allow to open a message in the browser
(add-to-list 'mu4e-view-actions
             '("ViewInBrowser" . mu4e-action-view-in-browser) t)

;; calendar setup
(setq calendar-week-start-day 1
      calendar-date-style "european"
      calendar-day-name-array ["Dimanche" "Lundi" "Mardi" "Mercredi"
                               "Jeudi" "Vendredi" "Samedi"]
      calendar-month-name-array ["Janvier" "Février" "Mars" "Avril" "Mai"
                                 "Juin" "Juillet" "Aout" "Septembre"
                                 "Octobre" "Novembre" "Décembre"]
      calendar-day-header-array ["Di" "Lu" "Ma" "Me" "Je" "Ve" "Sa"])

(copy-face font-lock-constant-face 'calendar-iso-week-face)
(set-face-attribute 'calendar-iso-week-face nil
                    :height 0.9)
(setq calendar-intermonth-text
      '(propertize
        (format "S%2d"
                (car
                 (calendar-iso-from-absolute
                  (calendar-absolute-from-gregorian (list month day year)))))
        'font-lock-face 'calendar-iso-week-face))

;; gpg configuration
(setq epg-pinentry-mode 'loopback)


;; ledger configuration
(defun jp-ledger/read-date ()
  "read a date and insert it."
  (interactive)
  (insert (format-time-string "%Y-%m-%d"
                      (let ((org-read-date-prefer-future nil))
                        (org-read-date nil t nil "Date: ")))))

(defun jp-ledger/open-file-at-point ()
  "Open existing file at point.
If «C-u» is used, use evince to open the file, otherwise the
other emacs frame.
"
  (interactive)
  (let ((file (thing-at-point 'existing-filename 'no-properties)))
    (if file
        (if (consp current-prefix-arg)
            (call-process "evince" nil 0 nil file)
          (view-file-other-window file))
      (org-open-at-point))))

(defun jp-ledger/insert-file-name ()
  (interactive)
  (let ((insert-default-directory nil) ;; not working!
        (fname nil)
        (bounds (bounds-of-thing-at-point 'filename)))
    (setq fname (helm-read-file-name
                 "File: "
                 :initial-input (if (consp bounds)
                                    (buffer-substring-no-properties (car bounds) (cdr bounds))
                                  default-directory)))
    (when (consp bounds)
      (delete-region (car bounds) (cdr bounds)))
    (insert "./"
            ;; so here is a hack to have a relative path...
            (file-relative-name fname default-directory))))

(defun jp-ledger/--lettrage-get-current (buffer)
  "Renvoie le dernier lettrage utilisé, ou une string vide."
  (let ((lettrage-result ""))
    (with-current-buffer buffer
      (save-excursion
        (goto-char (point-min))
        (while (re-search-forward "; +lettrage: *\\([A-Z]+\\)" nil t)
          (let ((lettrage (match-string 1)))
            (when (or (< (length lettrage-result) (length lettrage))
                      (string< lettrage-result lettrage))
              (setq lettrage-result lettrage))
            ))))
    lettrage-result))


(defun jp-ledger/--lettrage-next (lettrage)
  "Renvoie le lettrage suivant celui donné en paramètre.
Par exemple, AA -> AB."
  (if (string-empty-p lettrage)
      "A"
    (let ((last-char (substring lettrage -1))
          (alphabet "ABCDEFGHIJKLMNOPQRSTWXYZ"))
      (if (string= last-char "Z")
          (make-string (+ 1 (length lettrage)) ?A)
        (let ((pos (string-search last-char alphabet)))
          (concat (substring lettrage 0 -1) (substring alphabet (+ 1 pos) (+ 2 pos))))))))

(defun jp-ledger/lettrage-insert-current ()
  "Insère à la fin de la ligne courante le lettrage en cours."
  (interactive)
  (end-of-line)
  (insert "  ; lettrage: " (jp-ledger/--lettrage-get-current (current-buffer))))


(defun jp-ledger/lettrage-insert-next ()
  "Insère à la fin de la ligne courante le lettrage suivant."
  (interactive)
  (end-of-line)
  (insert "  ; lettrage: " (jp-ledger/--lettrage-next
                            (jp-ledger/--lettrage-get-current (current-buffer)))))

(defun jp-ledger/mode-hook ()
  (local-set-key (kbd "C-c .") 'jp-ledger/read-date)
  (local-set-key (kbd "C-c C-o") 'jp-ledger/open-file-at-point)
  (local-set-key (kbd "C-c C-f") 'jp-ledger/insert-file-name)
  (local-set-key (kbd "C-c , c") 'jp-ledger/lettrage-insert-current)
  (local-set-key (kbd "C-c , n") 'jp-ledger/lettrage-insert-next))

(setq ledger-default-date-format "%Y-%m-%d")

;; ledger-report avec les options de ligne de commande que j'utilise
(setq ledger-reports
      '(("bal" "%(binary) --pedantic --strict --check-payees --date-format '%Y-%m-%d' -f %(ledger-file) bal")
        ("reg" "%(binary) --pedantic --strict --check-payees --date-format '%Y-%m-%d' -f %(ledger-file) reg")
        ("payee" "%(binary) --pedantic --strict --check-payees --date-format '%Y-%m-%d' -f %(ledger-file) reg @%(payee)")
        ("account" "%(binary) --pedantic --strict --check-payees --date-format '%Y-%m-%d' -f %(ledger-file) reg %(account)")))

(add-hook 'ledger-mode-hook 'jp-ledger/mode-hook)

;; compilation regex pour le script de check de ledger
(require 'compile) ;; for compilation-error-regexp-alist
(add-to-list 'compilation-error-regexp-alist '("\\(WARNING\\|ERROR\\): \\(-> \\)?\\[\\([^:]+\\):\\([[:digit:]]+\\)\\]" 3 4))

;; org caldav, synchro org et calendrier
(require 'org-caldav)
(setq org-caldav-url "https://posteo.de:8443/calendars/julien.pages")
(setq org-caldav-inbox (expand-file-name "~/Documents/org/agenda/caldav.org"))
(setq org-caldav-calendar-id "default")
(setq org-caldav-files '("~/Documents/org/agenda/agenda.org" "~/Documents/org/agenda/agenda_archive.org"))

(setq avy-keys '(?a ?u ?i ?e ?, ?c ?t ?s ?r ?n ?m))
