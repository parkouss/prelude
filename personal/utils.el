;;  utilities

(defun jp/pass-show-extract (name)
  "Run the shell command `'pass show`' with the given name and
  return the output as a ALIST. The key for the primary password
  is 'password.
"
  (let ((result nil))
    (with-temp-buffer
      (shell-command (concat "pass show " name) (current-buffer))
      (goto-char (point-min))
      (push (cons 'password (buffer-substring-no-properties
                             (line-beginning-position)
                             (line-end-position))) result)
      (forward-line 1)
      (while (not (eobp))
        (search-forward ":")
        (push (cons
               (string-trim (buffer-substring-no-properties
                             (line-beginning-position) (match-beginning 0)))
               (string-trim (buffer-substring-no-properties
                             (match-end 0) (line-end-position)))) result)
        (forward-line 1))
      )
    result))


(defun jp/--org-contact-to-vevent (contact)
  "Generate icalendar data for an org-contact anniversary"
  (let ((birthday (cdr (assoc-string org-contacts-birthday-property (nth 2 contact))))
        (name (car contact)))
    (princ
     (format
      "BEGIN:VEVENT
UID: ANNIV-%s-%s
DTSTART:%s
SUMMARY:Anniversaire de %s
DESCRIPTION:Anniversaire de %s, né(e) en %s.
CATEGORIES:birthday
RRULE:FREQ=YEARLY
END:VEVENT\n"
      birthday (mapconcat 'identity (org-split-string name "[^a-zA-Z0-90]+") "-")
      (string-join (split-string birthday "-"))
      name
      name (car (split-string birthday "-"))))))

(defun jp/--org-contacts-to-vevents ()
  (mapcar #'jp/--org-contact-to-vevent
          (org-contacts-filter nil nil (cons org-contacts-birthday-property ""))))

;; override the bbdb hook to generate anniversaries from org contacts
;; (in combination with)
(advice-add 'org-bbdb-anniv-export-ical :override #'jp/--org-contacts-to-vevents)
(setq org-icalendar-include-bbdb-anniversaries t)

(defun jp/push-icalendar-agenda-org-file ()
  "Create an agenda .ics file from our org agenda files, and push
it to the public ftp server where google calendar knows how to find it.
"
  (interactive)
  (let ((credentials (jp/pass-show-extract "personal/infinityfree"))
        (org-icalendar-combined-agenda-file "/tmp/org.ics"))
    (org-icalendar-combine-agenda-files)
    (with-temp-buffer
      (insert "user ")
      (insert (cdr (assoc "user-agenda-jp" credentials)))
      (insert " ")
      (insert (cdr (assoc "pwd-agenda-jp" credentials)))
      (insert "\n")
      (insert (concat "put " org-icalendar-combined-agenda-file " " "/htdocs/org.ics"))
      (insert "\n")
      (call-process-region (point-min) (point-max) "ftp" nil nil nil "-inp" "ftpupload.net")))
  (message "org agenda exported as icalendar and pushed on the cloud."))

;; simple stuff for printing

(setq ps-print-header nil)  ; Don't print ps-print header (file name, date, ...)
;; Following custom margins seems to be pretty correct for me.
(setq ps-top-margin 100)
(setq ps-right-margin 35)
(setq ps-left-margin 48)
(setq ps-font-size '(7.5 . 9.0)) ; a bit more than default values

(defun jp/print-to-pdf ()
  "Print current buffer to pdf. No faces

For letters, try to start right-aligned text at around 79 chars.
"
  (interactive)
  (let ((buffer-fname (concat (buffer-file-name) ".pdf")))
    (save-current-buffer
      (ps-spool-buffer)
      (set-buffer "*PostScript*")
      (shell-command-on-region (point-min) (point-max)
                               (concat "ps2pdf - " (shell-quote-argument buffer-fname)))
      (kill-buffer))
    (message (concat "Saved to: " buffer-fname))))

;; org contacts vcard generation

(defun jp/org-contacts-to-json (&optional name-match tags-match prop-match)
  "Serialize org contacts to json"
  (json-encode (mapcar
                (lambda (contact) (elt contact 2))
                (org-contacts-filter name-match tags-match prop-match))))

(defun jp/org-contacts-export-as-vcard-internal (file &optional name-match tags-match prop-match)
  "Export vcard information to FILE for org contacts"
  (with-temp-buffer
    (insert (jp/org-contacts-to-json name-match tags-match prop-match))
    (call-process-region (point-min) (point-max)
                         (expand-file-name "personal/org-contacts-to-vcard.py" user-emacs-directory)
                         nil (list :file file) nil))
  (message (concat file " generated.")))

(defun jp/org-contacts-export-as-vcard ()
  "Export vcard information to FILE for org contacts"
  (interactive)
  (jp/org-contacts-export-as-vcard-internal org-contacts-vcard-file))

(jp/org-contacts-export-as-vcard)
