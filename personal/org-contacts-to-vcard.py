#!/usr/bin/python

import vobject
import re
import sys
import json


contacts = json.load(sys.stdin)

for contact in contacts:
    j = vobject.vCard()

    def addprop(name, value, **kwargs):
        if value is None:
            return
        p = j.add(name)
        p.value = value
        for k, v in kwargs.items():
            setattr(p, k, v)
        return p

    def addaddr(name, value, **kwargs):
        p = addprop(name, value, **kwargs)
        if p is not None:
            value = " ".join(p.value.split())
            r = re.match(r"(.+)\s+(\d+)\s+(.+)", value)
            if r:
                p.value = vobject.vcard.Address(
                    box=r.group(1).rstrip(","),
                    city=r.group(3), code=r.group(2))
            else:
                p.value = vobject.vcard.Address(box=value)
        return p

    fullname = contact["ITEM"]
    r = re.match(r"(.+)\s(.+)", fullname)
    if r:
        addprop("n", vobject.vcard.Name(family=r.group(1), given=r.group(2)))
    else:
        addprop("n", vobject.vcard.Name(family=fullname))

    addprop("fn", contact.get("ALIAS") or contact["ITEM"])

    addprop("email", contact.get("EMAIL"), type_param="INTERNET")

    addprop("tel", contact.get("PHONE"), type_param="CELL")
    addprop("tel", contact.get("PHONE_HOME"), type_param="HOME")
    addprop("tel", contact.get("PHONE_WORK"), type_param="WORK")

    addprop("bday", contact.get("BIRTHDAY"))

    addaddr("ADR", contact.get("ADDRESS") or contact.get("ADDRESS_HOME"),
            type_param="HOME")
    addaddr("ADR", contact.get("ADDRESS_WORK"), type_param="WORK")

    j.serialize(sys.stdout)
